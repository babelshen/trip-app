import Footer from 'components/Footer';
import Header from 'components/Header';
import MainBlock from 'components/MainBlock';

const App = () => (
    <>
      <Header />
      <MainBlock />
      <Footer />
    </>
  );

export default App;
