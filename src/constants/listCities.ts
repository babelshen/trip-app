import berlinImage from 'assets/cities/berlin.png';
import tokioImage from 'assets/cities/tokio.png';
import barcelonaImage from 'assets/cities/barcelona.png';
import londonImage from 'assets/cities/london.png';
import warsawImage from 'assets/cities/warsaw.png';
import parisImage from 'assets/cities/paris.png';
import madridImage from 'assets/cities/madrid.png';
import lisbonImage from 'assets/cities/lisbon.png';
import athensImage from 'assets/cities/athens.png';
import chisinauImage from 'assets/cities/chisinau.png';

export const listCities = [
  {
    id: 1,
    name: 'Berlin',
    image: berlinImage,
  },
  {
    id: 2,
    name: 'Tokyo',
    image: tokioImage,
  },
  {
    id: 3,
    name: 'Barcelona',
    image: barcelonaImage,
  },
  {
    id: 4,
    name: 'London',
    image: londonImage,
  },
  {
    id: 5,
    name: 'Warsaw',
    image: warsawImage,
  },
  {
    id: 6,
    name: 'Paris',
    image: parisImage,
  },
  {
    id: 7,
    name: 'Madrid',
    image: madridImage,
  },
  {
    id: 8,
    name: 'Lisbon',
    image: lisbonImage,
  },
  {
    id: 9,
    name: 'Athens',
    image: athensImage,
  },
  {
    id: 10,
    name: 'Chisinau',
    image: chisinauImage,
  },
];
