export const listSort = [
  {
    id: 1,
    name: 'none',
  },
  {
    id: 2,
    name: 'in ascending order of the start date',
  },
  {
    id: 3,
    name: 'in descending order of start date',
  },
];
