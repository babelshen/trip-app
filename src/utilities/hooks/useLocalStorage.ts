import { ICity } from 'components/interface';
import { useCallback, useState, useEffect } from 'react';

export function useLocalStorage(key: string, defaultValue: ICity[]) {
  return useStorage(key, defaultValue, window.localStorage);
}

function useStorage(key: string, defaultValue: ICity[], storageObject: Storage) {
  const [value, setValue] = useState(() => {
    const jsonValue = storageObject.getItem(key);
    if (jsonValue != null) return JSON.parse(jsonValue);
    return defaultValue;
  });
  useEffect(() => {
    if (value === undefined) return storageObject.removeItem(key);
    storageObject.setItem(key, JSON.stringify(value));
  }, [key, value, storageObject]);
  const remove = useCallback(() => {
    setValue(undefined);
  }, []);
  return [value, setValue, remove];
}
