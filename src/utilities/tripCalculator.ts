export const tripCalculator = (startDay: string) => {
  const [day, month, year] = startDay.split('.');
  const tripStartDate = new Date(Number(year), Number(month) - 1, Number(day));
  const currentDate = new Date();
  const difference = tripStartDate.getTime() - currentDate.getTime();

  const days = Math.floor(difference / (1000 * 60 * 60 * 24));
  const hours = Math.floor((difference % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  const minutes = Math.floor((difference % (1000 * 60 * 60)) / (1000 * 60));
  const seconds = Math.floor((difference % (1000 * 60)) / 1000);

  return [
    {
      name: 'Days',
      value: days,
    },
    {
      name: 'Hours',
      value: hours,
    },
    {
      name: 'Minutes',
      value: minutes,
    },
    {
      name: 'Seconds',
      value: seconds,
    },
  ];
};
