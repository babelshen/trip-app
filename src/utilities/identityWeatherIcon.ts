import clearDayIcon from '../assets/clear-day.png';
import clearNightIcon from '../assets/clear-night.png';
import cloudyIcon from '../assets/cloudy.png';
import partlyCloudyDayIcon from '../assets/partly-cloudy-day.png';
import partlyCloudyNightIcon from '../assets/partly-cloudy-night.png';
import rainIcon from '../assets/rain.png';
import showersDayIcon from '../assets/showers-day.png';
import showersNightIcon from '../assets/showers-night.png';
import snowIcon from '../assets/snow.png';
import thunderRain from '../assets/thunder-rain.png';
import windIcon from '../assets/wind.png';

export const identityWeatherIcon = (iconName: string) => {
  switch (iconName) {
    case 'clear-day':
      return clearDayIcon;
    case 'clear-night':
      return clearNightIcon;
    case 'cloudy':
      return cloudyIcon;
    case 'partly-cloudy-day':
      return partlyCloudyDayIcon;
    case 'partly-cloudy-night':
      return partlyCloudyNightIcon;
    case 'rain':
      return rainIcon;
    case 'showers-day':
      return showersDayIcon;
    case 'showers-night':
      return showersNightIcon;
    case 'snow':
      return snowIcon;
    case 'thunder-rain':
      return thunderRain;
    case 'wind':
      return windIcon;
    default:
      return '';
  }
};
