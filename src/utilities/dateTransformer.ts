import { IDateTransform } from 'components/interface';

export const dateTransformer = (date: string): string | IDateTransform => {
  if (date.indexOf('.') > 0) {
    const parts = date.split('.');
    return `${parts[2]}-${parts[1]}-${parts[0]}`;
  }
  if (date.indexOf('-') > 0) {
    const day = String(new Date(date).getDate());
    const month = String(new Date(date).getMonth() + 1);
    const year = String(new Date(date).getFullYear());
    return {
      day,
      month,
      year,
    };
  }
  return '';
};
