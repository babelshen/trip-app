import axios from 'axios';

export const getWeatherToday = async (name: string) => {
  try {
    const result = await axios.get(
      `https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/${name}/today?unitGroup=metric&include=days&key=${import.meta.env.VITE_APP_KEY}&contentType=json`,
    );
    return result.data.days[0];
  } catch (err) {
    throw new Error();
  }
};
