import axios from 'axios';
import { ICity } from 'components/interface';
import { dateTransformer } from 'utilities/dateTransformer';

export const getWeatherTrip = async (city: ICity) => {
  try {
    const startDay = dateTransformer(city.startDay);
    const endDay = dateTransformer(city.endDay);
    const result = await axios.get(
      `https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/${city.name}/${startDay}/${endDay}?unitGroup=metric&include=days&key=${import.meta.env.VITE_APP_KEY}&contentType=json`,
    );
    return result.data.days;
  } catch (err) {
    throw new Error();
  }
};
