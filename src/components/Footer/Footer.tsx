import style from './Footer.module.scss';

const Footer: React.FC = () => (
    <footer className={style.footer}>
      <div className={style.footer__wrapper}>
        <p>Made by Illia Babelnyk</p>
      </div>
    </footer>
  );

export default Footer;
