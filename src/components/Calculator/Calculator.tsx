import { tripCalculator } from 'utilities/tripCalculator';
import style from './Calculator.module.scss';
import { ICalculator } from './interface';

const Calculator: React.FC<ICalculator> = ({ city }) => {
  const calculatorDisplay = tripCalculator(city.startDay);

  return (
    <div className={style.calculator__wrapper}>
      {calculatorDisplay.map((item) => (
        <div key={item.name} className={style.item__wrapper}>
          <span className={style.item__value}>{item.value}</span>
          <span className={style.item__name}>{item.name}</span>
        </div>
      ))}
    </div>
  );
};

export default Calculator;
