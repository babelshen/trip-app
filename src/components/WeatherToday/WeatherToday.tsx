import { identityDayOfWeek } from 'utilities/identityDayOfWeek';
import { useEffect, useState } from 'react';
import { identityWeatherIcon } from 'utilities/identityWeatherIcon';
import Calculator from 'components/Calculator';
import { getWeatherToday } from 'api/getWeatherToday';
import CustomLoading from 'components/UI/CustomLoading';
import { IWeather } from 'components/interface';
import { IWeatherToday } from './interface';
import style from './WeatherToday.module.scss';

const WeatherToday: React.FC<IWeatherToday> = ({ city }) => {
  const [weatherToday, setWeatherToday] = useState<null | IWeather>(null);
  const [errorWeatherToday, setErrorWeatherToday] = useState<boolean>(false);
  const [loadingWeatherToday, setLoadingWeatherToday] = useState<boolean>(true);

  useEffect(() => {
    setLoadingWeatherToday(true);
    getWeatherToday(city.name)
      .then((result) => {
        setWeatherToday(result);
        setLoadingWeatherToday(false);
      })
      .catch(() => {
        setErrorWeatherToday(true);
        setLoadingWeatherToday(false);
      });
  }, [city]);

  const dayOfWeek = identityDayOfWeek(new Date().toDateString());

  if (errorWeatherToday) {
    return <span className={style.weather_today__error}>Something happened while downloading the data. Try again later.</span>;
  }

  if (loadingWeatherToday) {
    return <CustomLoading />;
  }

  if (weatherToday) {
    const weatherIcon = identityWeatherIcon(weatherToday.icon);

    return (
      <div className={style.wrapper}>
        <div className={style.weather_today__wrapper}>
          <span className={style.weather_today__day}>{dayOfWeek}</span>
          <div className={style.weather_today__forecast_display}>
            <div className={style.weather_today__image_wrapper}>
              <img src={weatherIcon} className={style.weather_today__image} alt="Weather icon" title={weatherToday.icon} />
            </div>
            <span className={style.weather_today__temperature}>
              {weatherToday.tempmax}
              <span className={style.weather_today__degree}>°C</span>
            </span>
          </div>
          <span className={style.weather_today__city_name}>{city.name}</span>
        </div>
        <Calculator city={city} />
      </div>
    );
  }
};

export default WeatherToday;
