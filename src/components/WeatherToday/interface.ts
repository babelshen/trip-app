import { ICity } from 'components/interface';

export interface IWeatherToday {
  city: ICity;
}
