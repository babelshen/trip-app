import { useState } from 'react';
import { useForm } from 'react-hook-form';
import CustomSelect from 'components/UI/CustomSelect';
import CustomButton from 'components/UI/CustomButton';
import { listCities } from 'constants/listCities';
import { dateTransformer } from 'utilities/dateTransformer';
import { ICity, ICreateCityTrip, IDataModalForm, IDateTransform } from 'components/interface';
import { IModalForm } from './interface';
import style from './ModalForm.module.scss';
import CustomInput from '../UI/CustomInput';

const ModalForm: React.FC<IModalForm> = ({ setIsOpenModal, setCitiesStorage }) => {
  const {
    register,
    handleSubmit,
    formState: { errors },
    watch,
    reset,
  } = useForm<IDataModalForm>({ mode: 'all' });
  const [city, setCity] = useState<ICreateCityTrip | null>(null);
  const [inputTypeStartDate, setInputTypeStartDate] = useState<string>('text');
  const [inputTypeEndDate, setInputTypeEndDate] = useState<string>('text');
  const [errorCity, setErrorCity] = useState<boolean>(false);

  const onSubmit = (data: IDataModalForm) => {
    if (city) {
      const startDay = dateTransformer(data.startDay) as IDateTransform;
      const endDay = dateTransformer(data.endDay) as IDateTransform;
      const result = {
        ...city,
        startDay: `${startDay.day}.${Number(startDay.month) < 10 ? `0${  startDay.month}` : startDay.month}.${startDay.year}`,
        endDay: `${endDay.day}.${Number(endDay.month) < 10 ? `0${  endDay.month}` : endDay.month}.${endDay.year}`,
      } as ICity;
      setIsOpenModal(false);
      setCitiesStorage((prev) => [...prev, result]);
    } else {
      setErrorCity(true);
    }
  };

  const today = new Date().toISOString().split('T')[0];
  const maxDate = new Date();
  maxDate.setDate(maxDate.getDate() + 15);
  const maxDateString = maxDate.toISOString().split('T')[0];

  const startDateValue = watch('startDay');
  let maxEndDate;
  if (startDateValue) {
    const startDateObj = new Date(startDateValue);
    const endDateObj = new Date(startDateObj.getTime() + 15 * 24 * 60 * 60 * 1000);
    maxEndDate = endDateObj.toISOString().split('T')[0];
  } else {
    maxEndDate = '';
  }

  const handleCancel = () => {
    reset();
    setCity(null);
  };

  return (
    <form className={style.form} onSubmit={handleSubmit(onSubmit)}>
      <div className={style.form__wrapper}>
        <span className={style.select__label}>
          <span className={style.select__require}>*</span>City
        </span>
        <CustomSelect city={city as ICreateCityTrip} setCity={setCity} listCities={listCities} placeholder="Please select a city" />

        {errorCity ? <p className={style.form__error_message}>You must choose city</p> : false}
        <CustomInput
          type={inputTypeStartDate}
          placeholder="Select date"
          error={errors.startDay}
          id="startday"
          label="Start day"
          onFocus={() => setInputTypeStartDate('date')}
          {...register('startDay', {
            required: 'You need to choose a date',
            min: {
              value: today,
              message: 'You cannot select a day that is already passed',
            },
            max: {
              value: maxDateString,
              message: 'You cannot select a date more than 15 days later.',
            },
          })}
        />

        <CustomInput
          type={inputTypeEndDate}
          placeholder="Select date"
          error={errors.endDay}
          id="endday"
          label="End day"
          onFocus={() => setInputTypeEndDate('date')}
          {...register('endDay', {
            required: 'You need to choose a date',
            min: {
              value: watch('startDay') || today,
              message: 'End date cannot be earlier than start date',
            },
            max: {
              value: maxEndDate,
              message: 'End date cannot be more than 15 days after start date.',
            },
          })}
        />
      </div>

      <div className={style.button_section}>
        <CustomButton styleFormat={false} onClick={handleCancel}>
          Cancel
        </CustomButton>
        <CustomButton styleFormat onClick={handleSubmit(onSubmit)}>
          Save
        </CustomButton>
      </div>
    </form>
  );
};

export default ModalForm;
