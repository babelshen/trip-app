import { ICity } from 'components/interface';
import { Dispatch, SetStateAction } from 'react';

export interface IModalForm {
  setIsOpenModal: (param: boolean) => void;
  setCitiesStorage: Dispatch<SetStateAction<ICity[]>>;
}
