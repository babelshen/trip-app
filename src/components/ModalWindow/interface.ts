import { ICity } from 'components/interface';
import { Dispatch, SetStateAction } from 'react';

export interface IModalWindow {
  isOpenModal: boolean;
  setIsOpenModal: (param: boolean) => void;
  setCitiesStorage: Dispatch<SetStateAction<ICity[]>>;
}
