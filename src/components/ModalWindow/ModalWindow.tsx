import { createPortal } from 'react-dom';
import { useEffect, useRef } from 'react';
import ModalForm from 'components/ModalForm';
import closeIcon from 'assets/close.png';
import { IModalWindow } from './interface';
import style from './ModalWindow.module.scss';

const ModalWindow: React.FC<IModalWindow> = ({ isOpenModal, setIsOpenModal, setCitiesStorage }) => {
  const modal = useRef<HTMLDialogElement | null>(null);

  useEffect(() => {
    if (isOpenModal) modal.current?.showModal();
    else modal.current?.close();
  }, [isOpenModal]);

  const modalRoot = document.getElementById('modal');
  if (!modalRoot) return null;

  return createPortal(
    <dialog className={style.modal} ref={modal}>
      <div className={style.modal__title}>
        <h2 className={style.modal__title_text}>Create trip</h2>
        <button type="button" onClick={() => setIsOpenModal(false)} className={style.modal__button_close}>
          <img src={closeIcon} className={style.modal__button_image} alt="Close window icon" title="Close window" />
        </button>
      </div>
      <ModalForm setIsOpenModal={setIsOpenModal} setCitiesStorage={setCitiesStorage} />
    </dialog>,
    modalRoot,
  );
};

export default ModalWindow;
