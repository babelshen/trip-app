import { useState } from 'react';
import Profile from 'components/Profile';
import style from './Header.module.scss';
import { IProfile } from './interface';

const Header: React.FC = () => {
  const [profile, setProfile] = useState<IProfile | null>(null);

  return (
    <header className={style.header}>
      <div className={style.header__wrapper}>Hello, {profile ? <strong>{profile.name}</strong> : <span className={style.header__nickname}>Guest!</span>}</div>
      <Profile setProfile={setProfile} profile={profile} />
    </header>
  );
};

export default Header;
