export interface ICity {
  id: number;
  image: string;
  name: string;
  startDay: string;
  endDay: string;
}

export interface IDataModalForm {
  startDay: string;
  endDay: string;
}

export interface IDateTransform {
  day: string;
  month: string;
  year: string;
}

export interface ICreateCityTrip {
  id: number;
  name: string;
  image?: string;
}

export interface ISortCity {
  id: number;
  name: string;
}

export interface IWeather {
  datetime: string;
  icon: string;
  tempmax: number;
  tempmin: number;
  [propName: string]: string | number;
}
