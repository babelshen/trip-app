import TripItemButton from 'components/UI/TripItemButton';
import AddTripButton from 'components/UI/AddTripButton';
import CustomScrollButton from 'components/UI/CustomScrollButton';
import { useRef } from 'react';
import { dateTransformer } from 'utilities/dateTransformer';
import style from './TripList.module.scss';
import { ITripCity } from './interface';

const TripList: React.FC<ITripCity> = ({ setCity, search, sortCity, citiesStorage, setIsOpenModal }) => {
  const listRef = useRef(null);

  let sortedListCities = [...citiesStorage];

  if (search) {
    sortedListCities = sortedListCities.filter((city) => city.name.toLowerCase().includes(search.toLowerCase()));
  }

  if (sortCity) {
    if (sortCity.name === 'in ascending order of the start date') {
      sortedListCities.sort((a, b) => new Date(dateTransformer(a.startDay) as string).getTime() - new Date(dateTransformer(b.startDay) as string).getTime());
    } else if (sortCity.name === 'in descending order of start date') {
      sortedListCities.sort((a, b) => new Date(dateTransformer(b.startDay) as string).getTime() - new Date(dateTransformer(a.startDay) as string).getTime());
    }
  }

  return (
    <div className={style.trip_list__container}>
      <div className={style.trip_list__wrapper} ref={listRef}>
        {sortedListCities.map((city) => (
          <TripItemButton key={city.name} setCity={setCity} city={city} />
        ))}
        <AddTripButton setIsOpenModal={setIsOpenModal} />
      </div>
      <div className={style.trip_list__buttons}>
        <CustomScrollButton direction="left" listRef={listRef} />
        <CustomScrollButton direction="right" listRef={listRef} />
      </div>
    </div>
  );
};

export default TripList;
