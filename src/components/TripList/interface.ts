import { ICity, ISortCity } from 'components/interface';

export interface ITripCity {
  setCity: (param: ICity) => void;
  search: string;
  sortCity: null | ISortCity;
  citiesStorage: ICity[];
  setIsOpenModal: (param: boolean) => void;
}
