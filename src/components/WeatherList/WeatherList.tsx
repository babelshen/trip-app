import WeatherItem from 'components/WeatherItem';
import { useEffect, useRef, useState } from 'react';
import CustomScrollButton from 'components/UI/CustomScrollButton';
import { getWeatherTrip } from 'api/getWeatherTrip';
import CustomLoading from 'components/UI/CustomLoading';
import { IWeather } from 'components/interface';
import { IWeatherList } from './interface';
import style from './WeatherList.module.scss';

const WeatherList: React.FC<IWeatherList> = ({ city }) => {
  const [weather, setWeather] = useState<null | IWeather[]>(null);
  const [loadingWeatherList, setLoadingWeatherList] = useState<boolean>(true);
  const [errorWeatherList, setErrorWeatherList] = useState<boolean>(false);

  useEffect(() => {
    setLoadingWeatherList(true);
    getWeatherTrip(city)
      .then((result) => {
        setWeather(result);
        setLoadingWeatherList(false);
      })
      .catch(() => {
        setLoadingWeatherList(false);
        setErrorWeatherList(true);
      });
  }, [city]);

  const listRef = useRef(null);

  if (errorWeatherList) {
    return <span className={style.weather_list__error}>Something happened while downloading the data. Try again later.</span>;
  }

  if (loadingWeatherList) {
    return <CustomLoading />;
  }

  if (weather) {
    return (
      <>
        <div className={style.weather_list__wrapper} ref={listRef}>
          {weather.map((dayInfo) => (
            <WeatherItem key={dayInfo.datetime} dayInfo={dayInfo} />
          ))}
        </div>
        <div className={style.weather_list__buttons}>
          <CustomScrollButton direction="left" listRef={listRef} />
          <CustomScrollButton direction="right" listRef={listRef} />
        </div>
      </>
    );
  } 
    return false;
  
};

export default WeatherList;
