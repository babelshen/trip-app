import { ICity } from 'components/interface';

export interface IWeatherList {
  city: ICity;
}
