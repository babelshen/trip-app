import { IProfile } from 'components/Header/interface';

export interface IResponse {
  access_token: string;
  authuser: string;
  expires_in: number;
  prompt: string;
  scope: string;
  token_type: string;
}

export interface IProfileComponent {
  profile: IProfile | null;
  setProfile: (param: IProfile | null) => void;
}
