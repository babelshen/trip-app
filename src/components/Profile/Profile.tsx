import { useState, useEffect } from 'react';
import { GoogleLogin, googleLogout, useGoogleLogin } from '@react-oauth/google';
import { googleLogin } from 'api/googleLogin';
import CustomButton from 'components/UI/CustomButton';
import { IProfileComponent, IResponse } from './interface';
import style from './Profile.module.scss';

const Profile: React.FC<IProfileComponent> = ({ profile, setProfile }) => {
  const [user, setUser] = useState<IResponse | null>(null);
  const [popupBlocked, setPopupBlocked] = useState<boolean>(false);

  const login = useGoogleLogin({
    onSuccess: (codeResponse) => setUser(codeResponse as IResponse),
    onError: (error) => console.log('Error:', error),
  });

  useEffect(() => {
    const checkPopupBlocker = () => {
      const newWindow = window.open('', 'test', 'height=1,width=1');
      if (!newWindow || newWindow.closed || typeof newWindow === 'undefined') {
        setPopupBlocked(true);
      } else {
        newWindow.close();
      }
    };

    checkPopupBlocker();
  }, []);

  useEffect(() => {
    if (user) {
      googleLogin(user.access_token)
        .then((res) => setProfile(res.data))
        .catch((err) => console.log('Error:', err));
    }
  }, [user, setProfile]);

  const logOut = () => {
    googleLogout();
    setProfile(null);
  };

  return (
    <div>
      {profile ? (
        <CustomButton onClick={logOut} styleFormat={false}>
          Log out
        </CustomButton>
      ) : (
        <div className={style.profile__login_block}>
          {popupBlocked ? <p className={style.profile__login_warning}>Please enable pop-ups to sign in with Google.</p> : false}
          <GoogleLogin onSuccess={() => login()} onError={() => console.log('Error:', 'Failed to open popup window')} />
        </div>
      )}
    </div>
  );
};

export default Profile;
