import { identityDayOfWeek } from 'utilities/identityDayOfWeek';
import { identityWeatherIcon } from 'utilities/identityWeatherIcon';
import { dateTransformer } from 'utilities/dateTransformer';
import { IDateTransform } from 'components/interface';
import style from './WeatherItem.module.scss';
import { IWeatherItem } from './interface';

const WeatherItem: React.FC<IWeatherItem> = ({ dayInfo }) => {
  const dayOfWeek = identityDayOfWeek(dayInfo.datetime);
  const weatherIcon = identityWeatherIcon(dayInfo.icon);
  const day = dateTransformer(dayInfo.datetime) as IDateTransform;

  return (
    <div key={dayInfo.datetime} className={style.weather_item__wrapper}>
      <span className={style.weather_item__day_week}>{dayOfWeek}</span>
      <span>
        {day.day}.{day.month}.{day.year}
      </span>
      <div className={style.weather_item__image_wrapper}>
        <img src={weatherIcon} className={style.weather_item__image} alt="Weather icon" title={dayInfo.icon} />
      </div>
      <span>
        {dayInfo.tempmax}° / {dayInfo.tempmin}°
      </span>
    </div>
  );
};

export default WeatherItem;
