export interface IWeatherItem {
  dayInfo: {
    datetime: string;
    icon: string;
    tempmax: number;
    tempmin: number;
  };
}
