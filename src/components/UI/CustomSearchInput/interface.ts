export interface ICustomSearchInput {
  search: string;
  setSearch: (param: string) => void;
  placeholder: string;
}
