import { useState } from 'react';
import searchIcon from '../../../assets/search.png';
import style from './CustomSearchInput.module.scss';
import { ICustomSearchInput } from './interface';

const CustomSearchInput: React.FC<ICustomSearchInput> = ({ search, setSearch, placeholder }) => {
  const [isFocused, setIsFocused] = useState<boolean>(false);

  return (
    <div className={`${style.input__wrapper} ${isFocused ? style.input__wrapper_focus : ''}`}>
      <div className={style.input__image_wrapper}>
        <img src={searchIcon} alt="Search" className={style.input__image} title="Search" />
      </div>
      <input
        className={style.input}
        type="text"
        placeholder={placeholder}
        value={search}
        onChange={(e) => setSearch(e.target.value)}
        onFocus={() => setIsFocused(true)}
        onBlur={() => setIsFocused(false)}
      />
    </div>
  );
};

export default CustomSearchInput;
