import React from 'react';
import { FieldError } from 'react-hook-form';
import style from './CustomInput.module.scss';

const CustomInput = React.forwardRef(
  (
    {
      type,
      id,
      label,
      placeholder,
      error,
      onFocus,
      ...props
    }: {
      type: string;
      id: string;
      label: string;
      placeholder: string;
      error?: FieldError;
      onFocus: () => void;
    },
    ref: React.Ref<HTMLInputElement>,
  ) => (
    <label htmlFor={id} className={style.input__label}>
      <span>
        <span className={style.input__require_marker}>*</span>
        {label}
      </span>
      <input className={style.input} type={type} placeholder={placeholder} id={id} ref={ref} onFocus={onFocus} {...props} />
      {error ? <p className={style.input__error}>{error.message}</p> : false}
    </label>
  ),
);

export default CustomInput;
