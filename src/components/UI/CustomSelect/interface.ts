import { ICreateCityTrip, ISortCity } from 'components/interface';

export interface ICustomSelect {
  city: ISortCity | ICreateCityTrip;
  // setCity: (param: ICreateCityTrip |ISortCity) => void;
  setCity: React.Dispatch<React.SetStateAction<ICreateCityTrip | null>>;
  listCities: ISortCity[] | ICreateCityTrip[];
  placeholder: string;
}
