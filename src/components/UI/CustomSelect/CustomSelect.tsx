import { useState } from 'react';
import style from './CustomSelect.module.scss';
import arrowIcon from '../../../assets/arrow.png';
import { ICustomSelect } from './interface';

const CustomSelect: React.FC<ICustomSelect> = ({ city, setCity, listCities, placeholder }) => {
  const [isOpenMenu, setIsOpenMenu] = useState<boolean>(false);

  return (
    <div className={style.select__wrapper}>
      <button type='button' className={style.select__button_wrapper} onClick={() => setIsOpenMenu(!isOpenMenu)}>
        {city ? <span className={style.select__option_choose}>{city.name}</span> : <span>{placeholder}</span>}
        <div className={style.select__button}>
          <img src={arrowIcon} className={isOpenMenu ? style.select__button_icon__open : style.select__button_icon__close} alt="Open menu icon" title="Choose city" />
        </div>

        {isOpenMenu ? (
          <div className={style.select__option_list}>
            {listCities.map((item) => (
              <button
                key={item.id}
                className={style.select__option}
                type="submit"
                onClick={() => {
                  setCity(item);
                  setIsOpenMenu(false);
                }}
              >
                {item.name}
              </button>
            ))}
          </div>
        ) : (
          false
        )}
      </button>
    </div>
  );
};

export default CustomSelect;
