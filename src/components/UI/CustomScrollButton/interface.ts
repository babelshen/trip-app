import { MutableRefObject } from 'react';

export interface ICustomScrollButton {
  direction: string;
  listRef: MutableRefObject<HTMLDivElement | null>;
}
