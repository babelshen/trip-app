import style from './CustomScrollButton.module.scss';
import { ICustomScrollButton } from './interface';

const CustomScrollButton: React.FC<ICustomScrollButton> = ({ direction, listRef }) => {
  const handleClick = () => {
    const scrollAmount = 100;

    if (direction === 'left') {
      listRef!.current!.scrollLeft -= scrollAmount;
    } else if (direction === 'right') {
      listRef!.current!.scrollLeft += scrollAmount;
    }
  };

  return (
    <button type='button' className={style.scroll__button} onClick={handleClick}>
      {direction === 'left' ? 'Left' : 'Right'}
    </button>
  );
};

export default CustomScrollButton;
