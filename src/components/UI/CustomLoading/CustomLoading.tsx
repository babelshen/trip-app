import style from './CustomLoading.module.scss';

const CustomLoading: React.FC = () => (
    <div className={style.spinner}>
      <div className={style.bounce1} />
      <div className={style.bounce2} />
      <div className={style.bounce3} />
    </div>
  );

export default CustomLoading;
