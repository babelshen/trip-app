import { ICity } from 'components/interface';

export interface ITripItemButton {
  city: ICity;
  setCity: (param: ICity) => void;
}
