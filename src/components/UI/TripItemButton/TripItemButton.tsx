import style from './TripItemButton.module.scss';
import { ITripItemButton } from './interface';

const TripItemButton: React.FC<ITripItemButton> = ({ setCity, city }) => (
    <button type="button" className={style.button__wrapper} onClick={() => setCity(city)}>
      <div className={style.button__image_wrapper}>
        <img className={style.button__image} src={city.image} alt={city.name} />
      </div>
      <div className={style.button__info}>
        <span className={style.button__info_name}>{city.name}</span>
        <span className={style.button__info_day}>
          {city.startDay} - {city.endDay}
        </span>
      </div>
    </button>
  );

export default TripItemButton;
