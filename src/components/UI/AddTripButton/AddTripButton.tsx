import plusIcon from 'assets/plus.png';
import style from './AddTripButton.module.scss';
import { IAddTripButton } from './interface';

const AddTripButton: React.FC<IAddTripButton> = ({ setIsOpenModal }) => (
    <button type="button" className={style.button} onClick={() => setIsOpenModal(true)}>
        <div className={style.button__image_wrapper}>
          <img src={plusIcon} alt="Add trip" className={style.button__image} />
        </div>
        <span className={style.button__text}>Add trip</span>
      </button>
  );

export default AddTripButton;
