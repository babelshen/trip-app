import { ReactNode } from 'react';

export interface ICustomButton {
  styleFormat: boolean;
  onClick: () => void;
  children: ReactNode;
}
