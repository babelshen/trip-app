import style from './CustomButton.module.scss';
import { ICustomButton } from './interface';

const CustomButton: React.FC<ICustomButton> = ({ styleFormat, onClick, children }) => (
    <button type="button" className={styleFormat ? style.button__color : style.button__general} onClick={onClick}>
      {children}
    </button>
  );

export default CustomButton;
