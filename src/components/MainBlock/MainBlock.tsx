import CustomSearchInput from 'components/UI/CustomSearchInput';
import TripList from 'components/TripList/TripList';
import WeatherList from 'components/WeatherList';
import { useState } from 'react';
import WeatherToday from 'components/WeatherToday/WeatherToday';
import CustomSelect from 'components/UI/CustomSelect';
import { listSort } from 'constants/listSort';
import berlinImage from 'assets/cities/berlin.png';
import ModalWindow from 'components/ModalWindow';
import { ICity, ISortCity } from 'components/interface';
import { useDebounce } from 'utilities/hooks/useDebounce';
import { useLocalStorage } from 'utilities/hooks/useLocalStorage';
import style from './MainBlock.module.scss';

const MainBlock: React.FC = () => {
  const [city, setCity] = useState<null | ICity>(null);
  const [search, setSearch] = useState<string>('');
  const [sortCity, setSortCity] = useState<null | ISortCity>(null);
  const [isOpenModal, setIsOpenModal] = useState<boolean>(false);

  const debouncedSearch = useDebounce(search, 500);
  const [citiesStorage, setCitiesStorage] = useLocalStorage('cities', [
    {
      id: 1,
      name: 'Berlin',
      image: berlinImage,
      startDay: '01.03.2024',
      endDay: '10.03.2024',
    },
  ] as ICity[]);

  return (
    <main className={style.main}>
      <ModalWindow isOpenModal={isOpenModal} setIsOpenModal={setIsOpenModal} setCitiesStorage={setCitiesStorage} />
      <section className={style.main__cities}>
        <h1 className={style.main__cities_title}>
          Weather <strong>Forecast</strong>
        </h1>
        <div className={style.main__cities_filter_wrapper}>
          <CustomSearchInput search={search} setSearch={setSearch} placeholder="Search your trip" />
          <CustomSelect city={sortCity as ISortCity} setCity={setSortCity} listCities={listSort} placeholder="Sort by" />
        </div>
        <TripList setCity={setCity} search={debouncedSearch} sortCity={sortCity} citiesStorage={citiesStorage} setIsOpenModal={setIsOpenModal} />
        <h2 className={style.main__weather_week__title}>Week</h2>
        {city ? <WeatherList city={city} /> : false}
      </section>
      {city ? (
        <section className={style.main__weather_today}>
          <WeatherToday city={city} />
        </section>
      ) : (
        false
      )}
    </main>
  );
};

export default MainBlock;
